#!/usr/bin/env python3
import smtplib
from pathlib import Path
import socks
import email
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import datetime as d
import zipfile as z
from config import * 

class Sender:
    def __init__(self, smtp, login, pass_, fromaddr, toaddrs, use_tls=False, proxy=None):
        self.smtp = smtp
        self.login = login
        self.pass_ = pass_
        self.fromaddr = fromaddr
        self.toaddrs = toaddrs
        if toaddrs is None:
            self.toaddrs = list()
        self.use_tls = use_tls
        self.proxy = proxy
        if self.proxy:
            proxy_host, proxy_port = self.proxy.split(':')
            socks.setdefaultproxy(socks.HTTP, proxy_host, int(proxy_port))
            socks.wrapmodule(smtplib)

    def mail(self, subject, message):
        if self.smtp:
            server = smtplib.SMTP(self.smtp)
            if self.use_tls: server.starttls()
            if self.login and self.pass_:
                server.login(self.login, self.pass_)

            msg = 'From: {}\r\nTo: {}\r\nSubject: {}\n\n{}'.format(
                self.fromaddr, self.toaddrs, subject, message
            )

            for toaddr in self.toaddrs:
                server.sendmail(self.fromaddr, toaddr, msg.encode('utf8'))
            server.quit()

    def mail_attach(self, subject, msg, attach, reportSails, status):
        message = MIMEMultipart()
        reportDate = d.datetime.now().strftime("%Y-%m-%d")
        message["From"] = self.fromaddr
        message["To"] = ', '.join(self.toaddrs)
        message["Subject"] = subject+" "+"за "+reportDate+" "+"00:00:00 ... 23:59:59"
        html_file = open(attach, "r")
        html_table = html_file.read()
        #html_table_file = attach
        count_sail = reportSails
        
        # message["Bcc"] = self.toaddrs[0]  # скрытая копия
        
        if status == 'done':
            html = """<html><head><style type="text/css"> table, th, td, tr{{border: 0px solid black; border-collapse: collapse; width:100%;}}</style></head>\
                <body><br> 1. Количество судов, от которых были получены сообщения {data_timer}: <b>{count_sail}</b><br/><br>{msg}<br/>""".format(\
                    subject=subject, msg=msg, count_sail=count_sail, data_timer=reportDate)
        if status == 'fail': 
            html = """<html><head><style type="text/css"> table, th, td, tr{{border: 0px solid black; border-collapse: collapse; width:100%; \
                }}</style></head><body><br> 1. Количество судов, от которых не приходили сообщения {data_timer}: <b>{count_sail}</b><br/><br>{msg}<br/>""".format(\
                    subject=subject, msg=msg, count_sail=count_sail, data_timer=reportDate)
        part1 = MIMEText(html, 'html')
        message.attach(MIMEText(msg, "plain"))
        message.attach(part1)
        part2 = MIMEText(html_table, 'html')
        message.attach(part2)
        with open(attach, "rb") as attachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())
        
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {Path(attach).name}",
        )


        message.attach(part)

        text = message.as_string()
        if self.smtp:
            server = smtplib.SMTP(self.smtp)
            if self.use_tls: server.starttls()
            if self.login and self.pass_:
                server.login(self.login, self.pass_)
            server.sendmail(self.fromaddr, self.toaddrs, text)
            server.quit()
            
