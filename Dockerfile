FROM python:3.8
ENV TZ=Europe/Moscow
COPY requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt
WORKDIR /srv
COPY . /srv
ENTRYPOINT ["python3", "alert_sender.py"]