#!/usr/bin/env python3
import logging
import os
import subprocess
from ..config import logger

class TilingTools:
    def __init__(self, tool='imagetiling', bins=None, logger_conf=None):
        if tool == 'imagetiling' or tool == 'copytiles':
            self.tool = tool
        else:
            raise ValueError(f'Tool "{tool}" not supported. Use "imagetiling" or "copytiles".')
        
        self.bins = bins
        if bins is None:
            self.bins = os.path.dirname(os.path.abspath(__file__))

        if logger_conf is None:
            logger_conf = logger
        logging.basicConfig(**logger_conf)
        self.logger = logging.getLogger('TilingTools')

    def run(self, timeout=1800, **kwargs):

        if 'i' not in kwargs:
            raise KeyError('Required key "i" not found.')
        if 'o' not in kwargs:
            raise KeyError('Required key "o" not found.')

        def dict_to_params(kwargs):
            params = list()
            for k, v in kwargs.items():
                if isinstance(v, list):
                    for item in v:
                        params.append('-' + k)
                        params.append(str(item))
                else:
                    params.append('-' + k)
                    params.append(str(v))
            return params

        exe = os.path.join(self.bins, f'{self.tool}.exe')
        args = [exe] + dict_to_params(kwargs)
        self._proc = subprocess.Popen(args=args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) # Merge stdout and stderr
        self.logger.info(f"Start - {kwargs['i']}")

        def handle_error(header=''):
            self._proc.kill()
            outs, errs = self._proc.communicate()
            log = outs.decode().rstrip()
            if os.path.isfile(kwargs['o']):
                os.remove(kwargs['o'])
            self.logger.error(f'{header} {log}')
            raise Exception(f'{header} {log}')

        try:
            outs, errs = self._proc.communicate(timeout=timeout)
            log = outs.decode().rstrip()
        except subprocess.TimeoutExpired:
            header = f'Timeout {timeout} sec'
            handle_error(header)
        else:
            if self._proc.returncode == 1:
                handle_error()
            else:
                self.logger.info(f"Successful - {kwargs['o']}")
                return kwargs['o'], log
        finally:
            del self._proc

    def kill(self):
        self._proc.kill()
        self.logger.info(f"Tool successful killed")
