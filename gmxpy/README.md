### Dependencies
Python 3.6+

TilingTools (https://github.com/ScanEx/TilingTools)

#### Examples Connecting TilingTools:
```python
from gmxpy import TilingTools
tool_1 = TilingTools(tool='imagetiling')  # gmxpy.tilingtools contains imagetiling.exe

# or
bins = 'C:/app/tilingtools'  # Directory with imagetiling.exe
tool_2 = TilingTools(tool='imagetiling', bins=bins)
```
