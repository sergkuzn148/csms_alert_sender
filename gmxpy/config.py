import logging

logger = {'handlers': [logging.StreamHandler()],
          'level':  logging.INFO,
          'format': '%(asctime)s - %(levelname)s - %(name)s - %(message)s',
          'datefmt': '%Y-%m-%d %H:%M:%S'}