#!/usr/bin/env python3
import logging
import time
import requests
from urllib.parse import urljoin
import json
from .config import logger

class GeomixerAPI:
    def __init__(self, user=None, password=None, api_url='https://maps.kosmosnimki.ru', logger_conf=None):
        self.session = requests.Session()
        self.api_url = api_url if api_url.endswith('/') else api_url + '/'
        
        if logger_conf is None:
            logger_conf = logger
        logging.basicConfig(**logger_conf)
        self.logger = logging.getLogger('GeomixerAPI')

        if user and password:
            self.login(user, password)

    def login(self, user, password):
        url = urljoin(self.api_url, 'Login.ashx')
        payload = {'login': user,
                   'pass': password}
        req = self.session.get(url, params=payload)
        res = self._check_response(req)
        self.logger.info(f'"{user}" is authorized.')
        return res

    def async_task(self, task_id, timeout=5):
        url = urljoin(self.api_url, 'AsyncTask.ashx')
        payload = {'TaskID': task_id}
        while True:
            req = self.session.get(url, params=payload)
            res = self._check_response(req)
            if res['Completed'] == True:
                if 'Result' in res:
                    self.logger.info(f'Task "{task_id}" successfully completed.')
                    return res['Result']
                else:
                    self.logger.error(res)
                    raise Exception(res)
                break
            else:
                self.logger.info(f"""Task "{task_id}" status {res['Status']}.""")
            time.sleep(timeout)

    def async_task_cancel(self, task_id):
        url = urljoin(self.api_url, 'AsyncTaskCancel')
        payload = {'TaskID': task_id}
        req = self.session.get(url, params=payload)
        res = self._check_response(req)
        if res is True:
            self.logger.info(f'Task "{task_id}" successfully canceled.')
            return True
        else:
            self.logger.info(f'Task "{task_id}" not found.')
            return False

    def layer_info(self, layer_id):
        url = urljoin(self.api_url, 'Layer/GetLayerInfo.ashx')
        payload = {'LayerID': layer_id}
        req = self.session.get(url, params=payload)
        res = self._check_response(req)
        self.logger.info(f'Layer "{layer_id}" information is obtained.')
        return res

    def layer_delete(self, layer_id):
        url = urljoin(self.api_url, 'Layer/Delete.ashx')
        payload = {'LayerID': layer_id}
        req = self.session.get(url, params=payload)
        res = self._check_response(req)
        self.logger.info(f'Layer "{layer_id}" successfully deleted.')
        return res

    def layer_security_add(self, layer_id, users):
        url = urljoin(self.api_url, 'Layer/UpdateSecurity.ashx')
        security_info = str({'UsersAdd':users})
        payload = {'LayerID': layer_id,
                   'SecurityInfo': security_info}
        req = self.session.get(url, params=payload)
        res = self._check_response(req)
        self.logger.info(f'Update security for layer "{layer_id}".')
        return res

    def raster_insert(self, title, path, border=None, meta=None):
        url = urljoin(self.api_url, 'RasterLayer/Insert.ashx')
        payload = {'Title': title, 
                   'TilePath': path}
        if border:
            payload['BorderFile'] = border

        if meta:
            def value_format(value):
                if isinstance(value, str):
                    return {'Value': str(value), 'Type': 'String'}
                raise TypeError('Dict meta does not support value '
                                f'with type {type(value).__name__}')
            meta_format = {}
            for key, value in meta.items():
                meta_format[key] = value_format(value)
            payload['MetaProperties'] = json.dumps(meta_format)
        
        req = self.session.get(url, params=payload)
        res = self._check_response(req)
        self.logger.info(f"""Insert raster. Task "{res['TaskID']}".""")
        return res

    def rc_search(self, layer_id, query=None, columns=None):
        url = urljoin(self.api_url, 'VectorLayer/Search.ashx')
        payload = {'layer': layer_id}
        if query:
            payload['query'] = str(query)
        if columns:
            payload['columns'] = str(columns)

        req = self.session.get(url, params=payload)
        res = self._check_response(req)

        key = res['fields']
        rows = res['values']
        if len(rows) > 0:
            found_list = list()
            for row in rows:
                found_list += [dict(zip(key, row))]
            self.logger.info(f'Search in layer "{layer_id}". Found records: {len(rows)}')
            return found_list
        else:
            self.logger.info(f'Search in layer "{layer_id}". No records found.')

    def rc_insert(self, rc_id, layer_id, properties=None):
        url = urljoin(self.api_url, 'VectorLayer/ModifyVectorObjects.ashx')
        props = {'GMX_RasterCatalogID': layer_id}
        if properties:
            props = {**props, **properties}
        objects = str([{'action': 'insert',
                        'properties': props}])
        payload = {'LayerName': rc_id, 
                   'Objects': objects}
        
        req = self.session.get(url, params=payload)
        res = self._check_response(req)
        self.logger.info(f'Insert in layer "{rc_id}". Object successfully added.')
        return res

    def rc_delete(self, rc_id, layer_id):
        url = urljoin(self.api_url, 'VectorLayer/ModifyVectorObjects.ashx')
        objects = str([{'action': 'delete',
                        'id': layer_id}])
        payload = {'LayerName': rc_id, 
                   'Objects': objects}

        req = self.session.get(url, params=payload)
        res = self._check_response(req)
        if 'ok' in res:
            self.logger.info(f'Deleting in layer "{rc_id}". Object "{layer_id}" successfully deleted.')
            return True
        else:
            self.logger.error(f'Could not delete object "{layer_id}" from layer "{rc_id}"')
            raise Exception(f'Could not delete object "{layer_id}" from layer "{rc_id}"')

    def map_insert(self, map_id, layer_id, visible=None, index=None, group=None):
        url = urljoin(self.api_url, 'Map/ModifyMap.ashx')
        objects = {'Action': 'insert',
                   'LayerName': layer_id}
        if visible is not None:
            if visible is True:
                objects['Visible'] = 'true'
            elif visible is False:
                objects['Visible'] = 'false'
        if index:
            objects['Index'] = index
        if group:
            objects['Group'] = group

        payload = {'MapName': map_id, 
                   'Objects': str([objects])}
        req = self.session.get(url, params=payload)
        res = self._check_response(req)
        if 'Ok' in res:
            self.logger.info(f'Insert in map "{map_id}". Layer "{layer_id}" successfully added.')
            return True
        else:
            self.logger.error(f'Could not insert layer "{layer_id}" from map "{map_id}"')
            raise Exception(f'Could not insert layer "{layer_id}" from map "{map_id}"')

    def download_layer(self, out_file, layer_id, format, columns):
        url = urljoin(self.api_url, 'DownloadLayer.ashx')
        data = {'t': layer_id,
                'format': format,
                'columns': columns}
        req = self.session.post(url, data=data)
        if req.status_code == 200 and req.content != b'':
            with open(out_file, 'wb') as f:
                f.write(req.content)
            self.logger.info(f'File "{out_file}" successfully downloaded.')
            return out_file
        else:
            raise Exception(f'Could not download file "{out_file}"')

    def _to_json(self, r):
        inp_str = r.text
        return json.loads(inp_str.strip('()'))

    def _check_response(self, r):
        try:
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            self.logger.error(f'HTTP Status {r.status_code} - {r.reason}')
            raise Exception(f'HTTP Status {r.status_code} - {r.reason}')
        except Exception as e:
            self.logger.error(e)
            raise Exception(e)
        else:
            j = self._to_json(r)
            # print(j)
            if j['Status'] == 'ok':
                return j['Result']
            else:
                self.logger.error(j)
                raise Exception(f"Response 'Status' not 'ok' - {j}")
