# csms_alert_sender

### Description
Сервис для отправки ежедневных отчетов .

### Dependencies
[gmxpy](https://github.com/perminovsi/gmxpy) (version 0.4.0) - Библиотека должен присутствовать в папке проекта перед сборкой image.

### Usage
- `dkr_build.sh` - Сборка образа для [registry.gitlab.com](gitlab.com)

- `dkr_create.sh` - Сборка контейнера.

Параметры конфигурации определяются в файле `config.py`

После изменения конфигурации запущенный ранее контейнер требуется перезапустить. 