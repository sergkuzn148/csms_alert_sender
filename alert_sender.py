import sender as sd
import gmxpy
import main as m
import logging
import time
import schedule
from config import *
import datetime as d
import csv
import zipfile as z
import psycopg2 as pg
import os
os.environ['PGOPTIONS'] = '-c statement_timeout=1605000'#1 час
import pandas as pd
  
#from sqlalchemy import create_engine

SVC_NAME = 'CSMS Sender'

def csms_alert(): 
    attempt = 1
    while attempt <= ATTEMPT['limit']:
        try:
            m.logger.info('Start csms_alert')
            reportDate = d.datetime.now().strftime("%Y-%m-%d")
            m.logger.info(reportDate)
            con = pg.connect(**DB['con_str'])
            m.logger.info('Connection ON')
            cur = con.cursor()
            m.logger.info('Cursor ON')
            #cur.execute("SET statement_timeout = 3600000")
            cur.execute(f"select _mmsi AS mmsi, _name AS name, _source AS source, _last_lat AS last_lat, _last_lon AS last_lon, _tais AS tais, \
                _sais AS sais,_last_msg_utc AS last_msg_utc, _branch AS branch FROM ais.create_csms_report('{reportDate}', FALSE)")
            rows = cur.fetchall()

            m.logger.info('Rows fetched')
            cur.close() 
            con.close()
            gr = getReport(rows)
            break
        except Exception as error:
            m.logger.error(error)
            attempt += 1
            if attempt <= ATTEMPT['limit']:
                time.sleep(ATTEMPT['timeout'])
            else:
                sndr = sd.Sender(**ALLERT['mail'], toaddrs=ALLERT['admins'])
                sndr.mail(f'{SVC_NAME}: ERROR', error)
        
def datedelta(l_date):
    dateToday = d.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
    last_date = l_date.strftime("%Y-%m-%d-%H:%M:%S")
    a = dateToday
    b = last_date
    a = a.split('-')
    b = b.split('-')
    aa = d.date(int(a[0]),int(a[1]),int(a[2]))
    bb = d.date(int(b[0]),int(b[1]),int(b[2]))
    cc = aa-bb
    dd = str(cc)
    return dd.split()[0]



def getReport(file):
    attempt = 1
    while attempt <= ATTEMPT['limit']:
        try:
            m.logger.info('Start csms_Report_done')
            html_done = '<table>'
            html_done +='<tr><th>#</th><th>Название</th><th>MMSI</th><th>T-AIS</th><th>S-AIS</th><th>Последнее местоположение</th><th>Филиал</th></tr>'
            count_done = 0
            
            html_fail = '<table>'
            html_fail +='<tr><th>#</th><th>Название</th><th>MMSI</th><th>Последнее сообщение (UTC)</th><th>АИС</th><th>Прошло дней</th><th>Последнее местоположение</th><th>Филиал</th></tr>'
            count_fail = 0 
            
            cnt_row_done = 0
            cnt_row_fail = 0
            
            for row in file:
                if (row[5] + row[6])!=0:
                    if cnt_row_done%2 == 0:
                        html_done+= '<tr style= background-color:#f2f2f2>'
                    elif cnt_row_done%2 != 0:
                        html_done+='<tr>'
                    cnt_row_done+=1
                    html_done +='<td>{id}</td><td>{name}</td><td>{mmsi}</td><td>{t_ais}</td>\
                        <td>{s_ais}</td><td>{last_pos}</td><td>{filial}</td>'.format(id=cnt_row_done,\
                            name=row[1], mmsi=row[0], t_ais=row[5], s_ais=row[6], \
                                last_pos='<a href={mapUrl} target=_blank>Просмотр на карте</a>'.format(\
                                    mapUrl = 'https://projects.scanex.ru/cfmc/?x={last_lon}&y={last_lan}&z={chis}&dt={date}'.format(\
                                        last_lon=row[4],last_lan=row[3],chis=13,date=d.date(int(row[7].strftime("%Y-%m-%d-%H:%M:%S").split('-')[0]),\
                                            int(row[7].strftime("%Y-%m-%d-%H:%M:%S").split('-')[1]),int(row[7].strftime("%Y-%m-%d-%H:%M:%S").split('-')[2])))),filial=row[8])
                    html_done +='</tr>'
                    count_done += 1
                elif (row[5] + row[6])==0:
                    if cnt_row_fail%2==0:
                        html_fail+= '<tr style= background-color:#f2f2f2>'
                    cnt_row_fail+=1
                    html_fail +='<td>{id}</td><td>{name}</td><td>{mmsi}</td><td>{date}</td>\
                        <td>{source}</td><td>{silence_days}</td><td>{last_pos}</td><td>{filial}</td>'.format(id=cnt_row_fail,\
                            name=row[1] if row[1]!=None else 'нет данных', mmsi=row[0],date=row[7] if row[7]!= None else 'нет данных',source=row[2] if row[2]!=None else 'нет данных',silence_days=datedelta(row[7]) if row[7]!=None else 'нет данных', \
                                last_pos='<a href={mapUrl} target=_blank>Просмотр на карте</a>'.format(\
                                    mapUrl = 'https://projects.scanex.ru/cfmc/?x={last_lon}&y={last_lan}&z={chis}&dt={date}'.format(\
                                        last_lon=row[4],last_lan=row[3],chis=13,date=d.date(int(row[7].strftime("%Y-%m-%d-%H:%M:%S").split('-')[0]),\
                                            int(row[7].strftime("%Y-%m-%d-%H:%M:%S").split('-')[1]),int(row[7].strftime("%Y-%m-%d-%H:%M:%S").split('-')[2]))if row[7]!=None else '')),filial=row[8])
                    html_fail +='</tr>'
                    count_fail += 1    
            m.logger.info('Tables were Finished')
            file_done = 'csms_done.html'
            with open(file_done, 'w') as fd:
                fd.write(html_done)
            file_fail = 'csms_fail.html'
            with open(file_fail, 'w') as ff:
                ff.write(html_fail)
            m.send(file_done, 'DoneMessage','doneSubject', count_done,'done')
            m.send(file_fail, 'FailMessage','failSubject', count_fail,'fail')

            break
        except Exception as error:
            m.logger.error(error)
            attempt += 1
            if attempt <= ATTEMPT['limit']:
                time.sleep(ATTEMPT['timeout'])
            else:
                sndr = sd.Sender(**ALLERT['mail'], toaddrs=ALLERT['admins'])
                sndr.mail(f'{SVC_NAME}: ERROR', error)


alert = m.logger.info('Start service...')
schedule.every().day.at(SCHEDULE).do(csms_alert)
while True:
    schedule.run_pending()
    time.sleep(1)

#alert = csms_alert()


