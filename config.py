SCHEDULE = '04:10'  # MSK


ATTEMPT = {
    'limit': 6,
    'timeout': 300  # second
}


DB = {
    'con_str': {
        'host': 'pgsql.kosmosnimki.ru',
        'database': 'maps',
        'user': 'kuznetsov',
        'password': 'AK4v{z45',
        'port': '5432',
        #'options': '-c statement_timeout=3600'
    }
}

GMX_API = {
    'user': 'skuznecov@scanex.ru',
    'password': 'Faust1488',
    'failDownload': {
        'layer_id': '983FC481C8C74FEDAFC82C6A5FB2D095',
        'format': 'csv',    
        'columns': '[{"Value":"[mmsi]","Alias":"mmsi"},{"Value":"[vessel_name]","Alias":"vessel_name"},{"Value":"[source]","Alias":"source"},{"Value":"[ts_pos_utc]","Alias":"ts_pos_utc"}]'
        
    },
    'doneDownload': {
        'layer_id': '983FC481C8C74FEDAFC82C6A5FB2D095',
        'format': 'csv',    
        'columns': '[{"Value":"[mmsi]","Alias":"mmsi"},{"Value":"[vessel_name]","Alias":"vessel_name"},{"Value":"[source]","Alias":"source"},{"Value":"[ts_pos_utc]","Alias":"ts_pos_utc"}]'
    },
    'download': {
        'layer_id': '983FC481C8C74FEDAFC82C6A5FB2D095',
        'format': 'csv',    
        'columns': '[{"Value":"[mmsi]","Alias":"mmsi"},{"Value":"[imo]","Alias":"imo"},{"Value":"[vessel_name]","Alias":"vessel_name"},{"Value":"[vessel_type]","Alias":"vessel_type"},{"Value":"[destination]","Alias":"destination"},{"Value":"[nav_status]","Alias":"nav_status"},{"Value":"[source]","Alias":"source"},{"Value":"[ts_pos_utc]","Alias":"ts_pos_utc"},{"Value":"[registry_name]","Alias":"registry_name"}]'
        #'columns': '[{"Value":"[mmsi]","Alias":"mmsi"},{"Value":"[vessel_name]","Alias":"vessel_name"},{"Value":"[source]","Alias":"source"},{"Value":"[ts_pos_utc]","Alias":"ts_pos_utc"}]'
    }
}

ALLERT = {
    'mail': {
        'proxy': "192.168.4.10:8080",
        'use_tls': False,
        'smtp': '192.168.5.167',
        'login': '',
        'pass_': '',
        'fromaddr': 'support@scanex.ru',
    },
    'subscribers': ['skuznecov@scanex.ru','slipatov@scanex.ru'],
    #'subscribers': ['skuznecov@scanex.ru', 'sergkuzn.kira@yandex.ru', 'kuznetsov.sd@phystech.edu'],
    'admins': ['kuznetsov.sd@phystech.edu']
}

LETTER = {
    'subject': 'ЦСМС АИС',
    'doneSubject': 'Отчет по принятым данным ЦСМС ',
    'failSubject': 'Отчет по непринятым данным ЦСМС',
    'message': 'Последние отметки судов.',
    'DoneMessage': '2. Информация о принятых сообщениях:',
    'FailMessage': '2. Список судов, от которых не приходило сообщений:',
    'filename': 'csms_ais_last_data.zip'
}
