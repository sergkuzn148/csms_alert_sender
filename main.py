#!/usr/bin/env python3
import logging
import time
import schedule
import gmxpy
from config import *
from sender import Sender

SVC_NAME = 'CSMS Alert'

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger('schedule').propagate = False
logger = logging.getLogger(SVC_NAME)

def download(status):
    logger.info('Download CSV...')
    api = gmxpy.GeomixerAPI()
    api.login(GMX_API['user'], GMX_API['password'])
    out_file = LETTER['filename']
    if status == 'fail':
        api.download_layer(**GMX_API['failDownload'], out_file=out_file)
    if status == 'done':
        api.download_layer(**GMX_API['doneDownload'], out_file=out_file)    
    if status == 'simple':
        api.download_layer(**GMX_API['download'], out_file=out_file)    
    logger.info('Done!')
    return out_file

def send(table, message, subject, sails, status):
    logger.info('Отправка...')
    sender = Sender(**ALLERT['mail'], toaddrs=ALLERT['subscribers'])
    sender.mail_attach(LETTER[subject], LETTER[message], table, sails, status)
    logger.info('Сообщение отправлено.')
    return True

def main():
    attempt = 1
    while attempt <= ATTEMPT['limit']:
        try:
            csv_file = download()
            send(csv_file)
            break
        except Exception as error:
            logger.error(error)
            attempt += 1
            if attempt <= ATTEMPT['limit']:
                time.sleep(ATTEMPT['timeout'])
            else:
                sender = Sender(**ALLERT['mail'], toaddrs=ALLERT['admins'])
                sender.mail(f'{SVC_NAME}: ERROR', error)


if __name__ == '__main__':
    logger.info('Start service...')
    schedule.every().day.at(SCHEDULE).do(main)
    while True:
        schedule.run_pending()
        time.sleep(1)
