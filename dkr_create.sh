#!/bin/bash
docker create \
--name csms_alert_sender \
--restart unless-stopped \
--log-opt max-size=5m \
--log-opt max-file=3 \
--mount type=bind,source="$(pwd)"/config.py,target=/srv/config.py \
registry.gitlab.com/sergkuzn148/csms_alert_sender